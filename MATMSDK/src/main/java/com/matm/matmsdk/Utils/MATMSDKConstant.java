package com.matm.matmsdk.Utils;

public class MATMSDKConstant {

    public static final int REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE = 1003;
    public static final int REQUEST_FOR_ACTIVITY_CASH_DEPOSIT_CODE = 1001;
    public static final int REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE = 1002;
    public static final int BALANCE_RELOAD = 1004;
    public static final String IIN_KEY = "IIN_KEY";
    public static final String TRANSACTION_STATUS_KEY = "TRANSACTION_STATUS_KEY";
    public static final String MICRO_ATM_TRANSACTION_STATUS_KEY = "MICRO_ATM_TRANSACTION_STATUS_KEY";
    public static final String TRANSACTION_STATUS_KEY_SDK = "TRANSACTION_STATUS_KEY_SDK";
    public static final String DATEPICKER_TAG = "datepicker";
    public static final String USER_TOKEN_KEY = "USER_TOKEN_KEY";
    public static final String NEXT_FRESHNESS_FACTOR = "NEXT_FRESHNESS_FACTOR";
    public static final String EASY_AEPS_PREF_KEY = "EASY_AEPS_PREF_KEY";
    public static final String EASY_AEPS_USER_LOGGED_IN_KEY = "EASY_AEPS_USER_LOGGED_IN_KEY";
    public static final String EASY_AEPS_USER_NAME_KEY = "EASY_AEPS_USER_NAME_KEY";
    public static final String encryptedString = "encryptedString";
    public static final String BASE_URL = "https://mobile.9fin.co.in/";
    //public static final String BASE_URL = "https://uatapps.iserveu.online/matm";



    public static String transactionAmount="0";
    public static String transactionType="";
    public static String paramA="";
    public static String paramB="";
    public static String paramC="";
    public static String encryptedData="";
    public static String balanceEnquiry="0";
    public static String cashWithdrawal="1";
    public static String transactionResponse="";
    public static String loginID ="";
    public static String responseData ="responseData";
    public static int REQUEST_CODE= 5;

    public static String tokenFromCoreApp ="";
    public static String userNameFromCoreApp ="";
    public static String applicationType ="";
}
