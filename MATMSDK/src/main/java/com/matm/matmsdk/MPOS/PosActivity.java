package com.matm.matmsdk.MPOS;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.matm.matmsdk.Adapters.Adapter.QuickPaymentAdapter;
import com.matm.matmsdk.Bluetooth.BluetoothActivity;
import com.matm.matmsdk.ChooseCard.ChooseCardActivity;
import com.matm.matmsdk.Dashboard.MainActivity;
import com.matm.matmsdk.Error.ErrorActivity;
import com.matm.matmsdk.Interface.MPOS.PosApiInterface;
import com.matm.matmsdk.Model.MPOS.PosTransResponse;
import isumatm.androidsdk.equitas.R;
import com.matm.matmsdk.Service.BankResponse;
import com.matm.matmsdk.Service.DataSetting;
import com.matm.matmsdk.Service.PaxConditional;
import com.matm.matmsdk.Service.PosServices;
import com.matm.matmsdk.Service.TempApiAuthFactory;
import com.matm.matmsdk.Utils.EnvData;
import com.matm.matmsdk.Utils.MATMSDKConstant;
import com.matm.matmsdk.Utils.PAXScreen;
import com.matm.matmsdk.Utils.ResponseConstant;
import com.matm.matmsdk.Utils.ResultEvent;
import com.matm.matmsdk.Utils.Session;
import com.matm.matmsdk.Utils.Tools;
import com.matm.matmsdk.Utils.TransactionResponseHandler;
import com.matm.matmsdk.aepsmodule.utils.AepsSdkConstants;
import com.matm.matmsdk.transaction_report.TransactionStatusActivity;
import com.paxsz.easylink.api.EasyLinkSdkManager;
import com.paxsz.easylink.api.ResponseCode;
import com.paxsz.easylink.device.DeviceInfo;
import com.paxsz.easylink.model.DataModel.DataType;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.matm.matmsdk.Service.PosServices.completeTransAndGetData;
import static com.matm.matmsdk.Service.PosServices.disTransFailed;
import static com.matm.matmsdk.Service.PosServices.setResponseData;
import static com.matm.matmsdk.Utils.EnvData.RequestPinBlockAuto;
import static com.matm.matmsdk.Utils.Tools.decimalToHexaAmountReversal;


public class PosActivity extends AppCompatActivity implements View.OnClickListener {

    public EditText amountText;
    public Button transSubmit;
    public String amount = "0";
    public EasyLinkSdkManager manager;
    private Handler handler;
    public PosServices posServices;
    private ExecutorService backgroundExecutor;
    int SetResponseCodeRes = 99999;
    private RecyclerView listQuickpayment;
    QuickPaymentAdapter quickPaymentAdapter;
    ArrayList<String> quickPaymentList = new ArrayList<>();
    private Button btnWithdraw,btn_enquiry;
    private Boolean withdraw_bool=false,balance_enq=false;
    String transaction_type="00";
    int PANSN=0;
    public static String tempTrack2Data="";
    public static String tempDeviceData="";
    public static String PINBLOCK="";
    public static String PANNO="";
    public static String cardType="";
    public static Integer RESULT_INT = 00;
    String pairedDeviceName ="6B409842";
    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    private Boolean location_flag= false;
    private String lactStr="0.0";
    private String LngStr="0.0";

   String transactionType,tokenStr;
    public static PosActivity instance;
    ProgressDialog loadingView;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pos);
       // setUpGClient();
        instance = PosActivity.this;
        posServices = new PosServices();
        session = new Session(PosActivity.this);
        loadingView = new ProgressDialog(PosActivity.this);

        if(MATMSDKConstant.applicationType.equalsIgnoreCase("CORE")){
            session.setUserToken(MATMSDKConstant.tokenFromCoreApp);
            session.setUsername(MATMSDKConstant.userNameFromCoreApp);

        }else {

            if (MATMSDKConstant.encryptedData.trim().length() != 0 && MATMSDKConstant.paramA.trim().length() != 0 && MATMSDKConstant.paramB.trim().length() != 0 && MATMSDKConstant.transactionType.trim().length() != 0 && MATMSDKConstant.loginID.trim().length() != 0) {
                loadingView.setCancelable(false);
                loadingView.setMessage("Please Wait..");

                getUserAuthToken();
                // retriveUserList();
            } else {
                showAlert(getResources().getString(R.string.error_msg));
            }
        }

        listQuickpayment = findViewById(R.id.listQuickpayment);

        quickPaymentAdapter = new QuickPaymentAdapter(quickPaymentList,PosActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(),3);
        listQuickpayment.setLayoutManager(mLayoutManager);
        listQuickpayment.setItemAnimator(new DefaultItemAnimator());
        listQuickpayment.setAdapter(quickPaymentAdapter);
       //Adding static fdata.
        quickPaymentList.add("100");
        quickPaymentList.add("200");
        quickPaymentList.add("300");
        quickPaymentList.add("500");
        quickPaymentList.add("1000");
        quickPaymentList.add("2000");
        quickPaymentList.add("3000");
        quickPaymentList.add("5000");
        quickPaymentList.add("10000");
        amountText = findViewById(R.id.posAmount);
        transSubmit = findViewById(R.id.doTransaction);

        btnWithdraw = findViewById(R.id.btnWithdraw);
        btnWithdraw.setOnClickListener(this);
        btn_enquiry = findViewById(R.id.btn_enquiry);
        btn_enquiry.setOnClickListener(this);

        register(this);
        manager = EasyLinkSdkManager.getInstance(PosActivity.this);
        handler = new Handler();
        backgroundExecutor = Executors.newFixedThreadPool(10, runnable -> {
            Thread thread = new Thread(runnable, "Background executor service");
            thread.setPriority(Thread.MIN_PRIORITY);
            thread.setDaemon(true);
            return thread;
        });


        pairedDeviceName = manager.getConnectedDevice().getDeviceName();
        String identifier =  manager.getConnectedDevice().getIdentifier();
        int  productId = manager.getConnectedDevice().getProductId();
        int venderId = manager.getConnectedDevice().getVendorId();

        pairedDeviceName = pairedDeviceName.replace("D180-","").trim();
        System.out.println("identifier"+identifier+"-----"+"productId"+productId+"-----------"+"venderId"+venderId);



    }




    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initView(){
        /*register(this);
        manager = EasyLinkSdkManager.getInstance(PosActivity.this);
        handler = new Handler();
        backgroundExecutor = Executors.newFixedThreadPool(10, runnable -> {
            Thread thread = new Thread(runnable, "Background executor service");
            thread.setPriority(Thread.MIN_PRIORITY);
            thread.setDaemon(true);
            return thread;
        });


        pairedDeviceName = manager.getConnectedDevice().getDeviceName();
        String identifier =  manager.getConnectedDevice().getIdentifier();
        int  productId = manager.getConnectedDevice().getProductId();
        int venderId = manager.getConnectedDevice().getVendorId();

        pairedDeviceName = pairedDeviceName.replace("D180-","").trim();
        System.out.println("identifier"+identifier+"-----"+"productId"+productId+"-----------"+"venderId"+venderId);*/

         tokenStr = MATMSDKConstant.USER_TOKEN_KEY;
         amount = MATMSDKConstant.transactionAmount;
         transactionType = MATMSDKConstant.transactionType;

        if(transactionType.equalsIgnoreCase("0")){
            transaction_type = "01";
            balance_enq = true;
        }else{
            transaction_type = "00";
            balance_enq = false;
        }

        if(balance_enq){
            amount="0";
            amount = String.valueOf(0);
            if(manager.isConnected(DeviceInfo.CommType.BLUETOOTH)){
               // this.finish();
                runInBackground(() -> PosActivity.this.cardTransaction(amount));
            }
            else{
                Intent intent = new Intent(PosActivity.this, BluetoothActivity.class);
                intent.putExtra("user_id",EnvData.user_id);
                if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                    Toast.makeText(getApplicationContext(),"Please Grant all the permissions", Toast.LENGTH_LONG).show();
                } else {
                    //intent.putExtra("token",tokenStr);
                    //intent.putExtra("transaction_amount",amount);
                    //intent.putExtra("balanceInquery",transactionType);
                    startActivity(intent);
                    finish();

                }
                Toast.makeText(getApplicationContext(),"Please go to setting and connect your bluetooth device.", Toast.LENGTH_LONG).show();
            }

        }else{
             amount = MATMSDKConstant.transactionAmount;
            if(Integer.parseInt(amount) > 0){
                if(manager.isConnected(DeviceInfo.CommType.BLUETOOTH)){
                    //this.finish();
                    runInBackground(() -> PosActivity.this.cardTransaction(amount));
                }
                else{
                    Intent intent = new Intent(PosActivity.this, BluetoothActivity.class);
                    intent.putExtra("user_id",EnvData.user_id);

                    if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                        Toast.makeText(getApplicationContext(),"Please Grant all the permissions", Toast.LENGTH_LONG).show();
                    } else {
                        //intent.putExtra("token",tokenStr);
                        //intent.putExtra("transaction_amount",amount);
                        //intent.putExtra("balanceInquery",transactionType);
                        startActivity(intent);
                        finish();

                    }}
            }
            else{
                Toast.makeText(getApplicationContext(),"Please enter a valid amount", Toast.LENGTH_LONG).show();
            }
        }



    }


     public static boolean isBlueToothConnected(Context context){
        EasyLinkSdkManager easyLinkSdkManager = EasyLinkSdkManager.getInstance(context);
         if(easyLinkSdkManager.isConnected(DeviceInfo.CommType.BLUETOOTH)){
             return true;
         }else{
             return false;
         }
     }


    //-----------------
   /* private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }*/



    public void updateQuikPaymentAdapter(String value){
        amountText.setText(value);
    }
    public void cardTransaction(String amount){
        //Starting the Card Showing Activity
        Intent cardShowActivity = new Intent(getApplicationContext(), ChooseCardActivity.class);
        startActivity(cardShowActivity);

        //................Debug mode

        manager.setDebugMode(true);//-------------Rajesh

        int getResponse;
        Integer transPrepareRet = transPrepare();

        if(transPrepareRet == 0){
            int response = manager.startTransaction(reportedData -> {
                if(reportedData!=null){

                    Log.d("MPOS","Report Data : " + new String(reportedData));
                    PosServices.doEvent(new ResultEvent(ResultEvent.Status.RECV_REPORT, new String(reportedData)));
                }
                return new byte[0];
            });

            if(response != ResponseCode.EL_RET_OK){

                Log.d("MPOS","GET ERROR : "+response);
                switch (response){
                    case 4046:
                        PAXScreen.showErrorOnPax(manager,response);
                        goToErrorPage(response);
                        //break;
                        return;
                    case 4001:
                        PAXScreen.showErrorOnPax(manager,response);
                        goToErrorPage(response);
                       // break;
                        return;
                    case 4003:
                        PAXScreen.showErrorOnPax(manager,response);
                        goToErrorPage(response);
                        //break;
                        return;
                    case 4006:
                        PAXScreen.showErrorOnPax(manager,response);
                        goToErrorPage(response);
                       // break;
                        return;
                         default:
                             goToErrorPage(response);
                             return;

                }


               // return;
            }
            //get enivernment of terminal

            ByteArrayOutputStream getCardEnvirnmentType = new ByteArrayOutputStream();
            getResponse = manager.getData(DataType.CONFIGURATION_DATA,new byte[]{(byte) 0x9C}, getCardEnvirnmentType);
            Log.d("MPOS", "get card envirnment: ret = " + getResponse + "  " + Tools.cleanByte(Tools.bcd2Str(getCardEnvirnmentType.toByteArray())));
            //get card type
            ByteArrayOutputStream getCardTypeTags = new ByteArrayOutputStream();
            getResponse = manager.getData(DataType.CONFIGURATION_DATA,new byte[]{0x03, 0x01}, getCardTypeTags);
            Log.d("MPOS", "get card type: ret = " + getResponse + "  " + Tools.cleanByte(Tools.bcd2Str(getCardTypeTags.toByteArray())));
            if (getResponse != ResponseCode.EL_RET_OK) {
                PosServices.errorHandel(getResponse,getApplicationContext());
                disTransFailed(getResponse);
                return;
            }

            Integer ret = transFlow(getCardTypeTags);
            if (ret != ResponseCode.EL_RET_OK) {
                ResponseCode.getRespCodeMsg(ret);
                disTransFailed(ret);
                return;
            }

        }
    }

    private void goToErrorPage(Integer response) {
        backgroundExecutor.shutdownNow();
        backgroundExecutor.shutdown();
        ChooseCardActivity.instance.finish();
        Intent intent = new Intent(PosActivity.this, ErrorActivity.class);
        intent.putExtra("errorResponse",response);
        startActivity(intent);
    }

    private int transPrepare(){
        String finalTransactionData = "";
        if(balance_enq==true){
            finalTransactionData = Tools.finalBalanceTranData("000000000000");
            // balance_enq=false;

        }else{
            String hexAmt = Tools.decimalToHexaAmount(amount,10);
            finalTransactionData = Tools.finalTranData(hexAmt);
        }

        Log.d("MPOS","Transaction Data : " + finalTransactionData);

        byte[] data = Tools.str2Bcd(finalTransactionData);
        ByteArrayOutputStream failedTags = new ByteArrayOutputStream();
        int transDataSetRet = manager.setData(DataType.TRANSACTION_DATA,data,failedTags);
        int confgDataSetRet = DataSetting.setAllData(manager);
        Log.d("MPOS","Set Data Transaction: " + transDataSetRet);
        Log.d("MPOS","Set Data Configuration: " + confgDataSetRet);

        if (confgDataSetRet != ResponseCode.EL_RET_OK && transDataSetRet != ResponseCode.EL_RET_OK) {
            disTransFailed(ResponseCode.EL_TRANS_RET_EMV_DENIAL);
            return ResponseCode.EL_TRANS_RET_EMV_DENIAL;

        }
        else {
            return ResponseCode.EL_RET_OK;
        }

    }

    private int transFlow(ByteArrayOutputStream cardType) {
        if ((cardType.toByteArray()[cardType.size() - 1] == 1) || (cardType.toByteArray()[cardType.size() - 1] == 2)) {
            //MSR card
            return goMSRBranch();
        } else {
            //not MSR card
            return goNotMSRBranch();
        }
    }

    private int goMSRBranch() {
        Log.d("MPOS","Find here");
        ByteArrayOutputStream failedTags = new ByteArrayOutputStream();
        byte[] configData = Tools.str2Bcd(RequestPinBlockAuto);
        int configRet = manager.setData(DataType.CONFIGURATION_DATA,configData,failedTags);
        if(configRet == ResponseCode.EL_RET_OK){
            cardType = "MSR";
            online(manager,"MSR");
            return 0;
        }
        else{
            return 4011;
        }
    }

    private int goNotMSRBranch() {
        Integer result = null;
        String cardProcessingRes = DataSetting.CardProcessingResult(manager);
        Log.d("MPOS","Card Processing Result"+ DataSetting.CardProcessingResult(manager));
        switch(cardProcessingRes){
            case "02":
                cardType="Chip";
                online(manager,"Chip");
            case "01":
                Toast.makeText(getApplicationContext(),"Transaction Approved", Toast.LENGTH_LONG).show();
                result = posServices.offline(manager);
                if (result != ResponseCode.EL_RET_OK) {
                    return result;
                }
                else {
                    return completeTransAndGetData(manager);
                }
            case "00":
                Toast.makeText(getApplicationContext(),"Transaction Declined", Toast.LENGTH_LONG).show();
                result = ResponseCode.EL_TRANS_RET_TRASN_DECLINED;
                if (result != ResponseCode.EL_RET_OK) {
                    return result;
                }
                break;
        }
        return result;
    }

    public void finish() {
        if(ChooseCardActivity.instance!=null){
            ChooseCardActivity.instance.finish();
        }else{
            Intent i = new Intent(PosActivity.this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

    }

    public void register(Object obj) {
        if (!EventBus.getDefault().isRegistered(obj)) {
            EventBus.getDefault().register(obj);
        }
    }

    public void unregister(Object obj) {
        if (EventBus.getDefault().isRegistered(obj)) {
            EventBus.getDefault().unregister(obj);
        }
    }

    public void runInBackground(final Runnable runnable) {
        backgroundExecutor.submit(runnable);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregister(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

       /* switch (v.getId()){
            case R.id.btnWithdraw:
                btnWithdraw.setTextColor(Color.parseColor("#FFFFFF"));
                btn_enquiry.setTextColor(Color.parseColor("#0367D3"));
                btnWithdraw.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_check));
                btn_enquiry.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_uncheck));
                withdraw_bool=true;
                balance_enq = false;
                transaction_type="00";
                amountText.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_enquiry:
                btnWithdraw.setTextColor(Color.parseColor("#0367D3"));
                btn_enquiry.setTextColor(Color.parseColor("#FFFFFF"));
                btnWithdraw.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_uncheck));
                btn_enquiry.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_check));
                transaction_type="01";
                balance_enq = true;
                withdraw_bool=false;
                amountText.setVisibility(View.GONE);
                break;
        }*/
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ResultEvent event) {
        switch ((ResultEvent.Status) event.getStatus()) {
            case SUCCESS:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                //transSuccess();
                break;
            case FAILED:
                finish();
                Toast.makeText(getApplicationContext(), ResponseCode.getRespCodeMsg((int) event.getData()), Toast.LENGTH_LONG);
                break;
            default:
                break;
        }
    }


    public void online(EasyLinkSdkManager manager, String Type) {

        try{

        JsonObject obj = null;
        PosApiInterface posApiInterface = TempApiAuthFactory.getTestClient().create(PosApiInterface.class);
        Call<PosTransResponse> call = null;
        if(Type.equalsIgnoreCase("Chip")){


            //Log.d("MPOS","PIN Block PIN Unsupported : " + DataSetting.getPinBlockPinUnsupported(manager));

            PINBLOCK = DataSetting.getPinBlock(manager);
         //   Log.d("MPOS","PIN Block : " + PINBLOCK);
         //   Log.d("MPOS","Get APP CARD VERSION : "+DataSetting.getAppVersionCode(manager));
         //   Log.d("MPOS","Get Card AID : " + DataSetting.cardAID(manager));
         //   Log.d("MPOS","Get Application Version Number : " + DataSetting.AppVersionNumber(manager));
         //   Log.d("MPOS","Get Card Acceptor Terminal Identification : " + DataSetting.TerminalIdentification(manager));
         //   Log.d("MPOS","Get Merchant Identification : " + DataSetting.CardAcceptorIdentification(manager));
         //   Log.d("MPOS","Get Transaction Type : " + DataSetting.TransactionType(manager));
          //  Log.d("MPOS","Get Authorized Amount : " + DataSetting.getAuthorizedAmount(manager));
          //  Log.d("MPOS","Get Local Time : " + DataSetting.getTransactionTime(manager));
          //  Log.d("MPOS","Get Card Sequence Number : " + DataSetting.cardSequenceNumber(manager));
          //  Log.d("MPOS","Track 2 Equivalent Data : " + DataSetting.track2Data(manager));

            //PANNO = DataSetting.getPANNo(manager);


             DeviceInfo deviceInfo = manager.getConnectedDevice();
           /* String s1 = deviceInfo.getIp();
            String s2 = deviceInfo.getIdentifier();
            String s3 = deviceInfo.getDeviceName();
            Log.d("MPOS","Device S1 : "+s1);*/
            PANSN = PaxConditional.checkPanDigit(manager);
            PAXScreen.showMsgBoxFun(manager);

            tempTrack2Data = DataSetting.track2Data(manager);
            tempDeviceData = DataSetting.getAllData(manager);
            obj = ApiJsonMap(DataSetting.getAuthorizedAmount(manager),DataSetting.cardSequenceNumber(manager),tempTrack2Data,pairedDeviceName,PINBLOCK,tempDeviceData,PANSN);
        //    PAXScreen.showMsgBoxFun(manager);

        }
        else {
          /*  ByteArrayOutputStream trak2ByteData = new ByteArrayOutputStream();
            Integer responseData = manager.getData(DataType.CONFIGURATION_DATA,new byte[]{(byte) 0x03, 0x05}, trak2ByteData);
            String trackData2 = Tools.cleanByte(Tools.bcd2Str(trak2ByteData.toByteArray()));
            trackData2 = trackData2.substring(6);




          *//*  byte data[] = {0x02,0x05};
            ByteArrayOutputStream respData = new ByteArrayOutputStream();
            Integer responseDataa = manager.encryptData(data, respData);
            String trackData2 = Tools.bcd2Str(trak2ByteData.toByteArray());*//*

            *//*DataSetting.track2Datafallback(manager);*//*


            //Track 2 equvalent data


            Log.d("MPOS","PIN Block : " + DataSetting.getPinBlock(manager));
            Log.d("MPOS","TRACK Dataa2 : "+trackData2);

            PAXScreen.showMsgBoxFun(manager);
           // Integer panSN_Fallback = 1;//PaxConditional.checkPanDigitFallback(manager);

           *//* String panNumber;
            ByteArrayOutputStream getPanNumber = new ByteArrayOutputStream();
            int response = manager.getData(DataType.TRANSACTION_DATA,new byte[]{(byte)0x5A},getPanNumber);
            //if(response == ResponseCode.EL_RET_OK){
                panNumber = Tools.cleanByte(Tools.bcd2Str(getPanNumber.toByteArray()));
                panNumber = panNumber.substring(4);
                Log.d("MPOS","Pan Sequence Number New :" + panNumber);*//*
           // }



           // obj = ApiJsonMapFallback("6B409832",amount,trackData2,DataSetting.getPinBlock(manager),0);*/


            String trackData2 = DataSetting.trackData(manager);
            Log.d("MPOS","TRACK Dataa2 : "+trackData2);

            PAXScreen.showMsgBoxFun(manager);
            // Integer panSN_Fallback = 1;//PaxConditional.checkPanDigitFallback(manager);

           String amountStr =  decimalToHexaAmountReversal(amount,10);

            obj = ApiJsonMapFallback(pairedDeviceName,amountStr,trackData2,DataSetting.getPinBlock(manager),0);

        }

        if(balance_enq==true){
            //For Balance Inquery
            call = posApiInterface.SendTransRequestBalanceInq(EnvData.token,obj);
            call.enqueue(new Callback<PosTransResponse>() {
                @Override
                public void onResponse(Call<PosTransResponse> call, Response<PosTransResponse> response) {
                    Log.d("SuccessAPI", String.valueOf(response.body()));
                    if(response.isSuccessful()){
                        PosTransResponse posTransResponse = response.body();
                        String status = posTransResponse.getStatus().toString();
                        String details = posTransResponse.getStatusDes().toString();


                        if(status.equalsIgnoreCase("-1")){
                        }

                        TransactionResponseHandler.responseHandler(details);
                        String AuthManager = "";
                        Log.d("MPOS","RESPONSE CODE : " + ResponseConstant.DE39);
                        if((ResponseConstant.DE39).equalsIgnoreCase("0000")){
                            AuthManager = "00";
                        }else{
                            AuthManager ="01";
                        }
                       // System.out.println(AuthManager);
                        SetResponseCodeRes = setResponseData(manager,ResponseConstant.DE39.substring(2),AuthManager,ResponseConstant.DE38,ResponseConstant.DE55);

                        int result = completeTransAndGetData(manager);
                        String str = ResponseConstant.DE54;
                        Log.d("MPOS","VALUE ON DE55 : " + ResponseConstant.DE55);
                        if(!str.isEmpty() && !str.equalsIgnoreCase("null") && str!=null) {
                            amount = ResponseConstant.DE54.substring(27);
                            String BalsanceType = amount.substring(0, 1);
                            amount = amount.substring(1);
                            if (BalsanceType.equalsIgnoreCase("C")) {
                                amount = "" + String.valueOf(Integer.parseInt(amount));
                            }
                            if(!amount.equalsIgnoreCase("0")) {
                                amount = amount.substring(0, amount.length() - 2);
                            }
                            Log.d("MPOS", amount);
                        }

                        if(ResponseConstant.DE39.substring(2).equalsIgnoreCase(String.valueOf(BankResponse.Approved))){
                            Log.d("MPOS", String.valueOf(result));

                           // ReversalTestApi(DataSetting.getAuthorizedAmount(manager),DataSetting.cardSequenceNumber(manager),DataSetting.track2Data(manager),"6B409832",DataSetting.getPinBlock(manager),DataSetting.getAllData(manager),PANSN,String.valueOf(BankResponse.Approved));

                           // backgroundExecutor.shutdownNow();
                            //backgroundExecutor.shutdown();

                            Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                            intent.putExtra("flag","success");
                            intent.putExtra("RRN_NO",ResponseConstant.DE37);
                            intent.putExtra("RESPONSE_CODE",ResponseConstant.DE39);
                            intent.putExtra("APP_NAME", DataSetting.applName(manager));
                            intent.putExtra("AID", DataSetting.cardAID(manager));
                            intent.putExtra("AMOUNT", amount);
                            intent.putExtra("MID", ResponseConstant.DE42);
                            intent.putExtra("TID", ResponseConstant.DE41);
                            intent.putExtra("TXN_ID", "1234567");
                            intent.putExtra("INVOICE", "1111111");
                            intent.putExtra("CARD_TYPE", convertHexToStringValue(DataSetting.applName(manager)));
                            intent.putExtra("APPR_CODE", "123456");
                            intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);
                            startActivity(intent);
                            finish();
                            shutdownAndAwaitTermination(backgroundExecutor);
                        }
                        else {
                           // backgroundExecutor.shutdownNow();
                           // backgroundExecutor.shutdown();

                            Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                            intent.putExtra("flag","failure");
                            intent.putExtra("RRN_NO",ResponseConstant.DE37);
                            intent.putExtra("RESPONSE_CODE",ResponseConstant.DE39);
                            intent.putExtra("APP_NAME", "RuPay Debit");
                            intent.putExtra("AID", DataSetting.cardAID(manager));
                            intent.putExtra("AMOUNT", amount);
                            intent.putExtra("MID", ResponseConstant.DE42);
                            intent.putExtra("TID", ResponseConstant.DE41);
                            intent.putExtra("TXN_ID", "1234567");
                            intent.putExtra("INVOICE", "1111111");
                            intent.putExtra("CARD_TYPE", convertHexToStringValue(DataSetting.applName(manager)));
                            intent.putExtra("APPR_CODE", "123456");
                            intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);
                            startActivity(intent);
                            finish();
                            shutdownAndAwaitTermination(backgroundExecutor);
                        }
                    }

                }

                @Override
                public void onFailure(Call<PosTransResponse> call, Throwable t) {
                    call.cancel();
                    Log.d("Failure", String.valueOf(t));
                }
            });
        }
        else{
            //for Withdraw
          //  System.out.println("TOKEN>>>>---"+EnvData.token);
            call = posApiInterface.SendTransRequest(EnvData.token,obj);
            call.enqueue(new Callback<PosTransResponse>() {
                @Override
                public void onResponse(Call<PosTransResponse> call, Response<PosTransResponse> response) {
                    Log.d("MPOS: SuccessAPI", String.valueOf(response.body()));
                    if(response.isSuccessful()){
                        PosTransResponse posTransResponse = response.body();
                        String details = posTransResponse.getStatusDes().toString();
                        Log.d("MPOS","RESPONSE DATA: "+details);
                        TransactionResponseHandler.responseHandler(details);
                        String AuthManager = "";
                        Log.d("MPOS","RESPONSE CODE : " + ResponseConstant.DE39);
                        if((ResponseConstant.DE39).equalsIgnoreCase("0000")){
                            AuthManager = "00";
                        }else{
                            AuthManager ="01";
                        }
                       // System.out.println(AuthManager);

                        if(cardType.equalsIgnoreCase("Chip")){

                            SetResponseCodeRes = setResponseData(manager,ResponseConstant.DE39.substring(2),AuthManager,ResponseConstant.DE38,ResponseConstant.DE55);
                            int result = completeTransAndGetData(manager);
                            RESULT_INT = result;
                        }
                        //SetResponseCodeRes = setResponseData(manager,ResponseConstant.DE39.substring(2),AuthManager,ResponseConstant.DE38,ResponseConstant.DE55);



                        if(ResponseConstant.DE39.substring(2).equalsIgnoreCase(String.valueOf(BankResponse.Approved))){
                            //int result = completeTransAndGetData(manager);
                           // Log.d("MPOS",String.valueOf(result));

                           if(RESULT_INT==4011){
                                //ReversalTestApi(DataSetting.getAuthorizedAmount(manager),DataSetting.cardSequenceNumber(manager),tempTrack2Data,pairedDeviceName,PINBLOCK,DataSetting.getAllDataReversal(manager),PANSN,"E1");
                               ReversalTestApi(ResponseConstant.DE11,DataSetting.getAuthorizedAmount(manager),DataSetting.cardSequenceNumber(manager),tempTrack2Data,pairedDeviceName,PINBLOCK,DataSetting.getAllDataReversal(manager),PANSN,"E1");
                            }
                           /*else if(result==4043){
                                ReversalTestApi(DataSetting.getAuthorizedAmount(manager),DataSetting.cardSequenceNumber(manager),tempTrack2Data,"6B409832",PINBLOCK,DataSetting.getAllDataReversal(manager),PANSN,"22");

                            }*/
                           else{
                               //ReversalTestApi(DataSetting.getAuthorizedAmount(manager),DataSetting.cardSequenceNumber(manager),tempTrack2Data,pairedDeviceName,PINBLOCK,DataSetting.getAllDataReversal(manager),PANSN,"21");

                             //  backgroundExecutor.shutdownNow();
                              // backgroundExecutor.shutdown();

                                Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                                intent.putExtra("flag","success");
                                intent.putExtra("RRN_NO",ResponseConstant.DE37);
                                intent.putExtra("RESPONSE_CODE",ResponseConstant.DE39);
                                intent.putExtra("APP_NAME", "RuPay Debit");
                                intent.putExtra("AID", DataSetting.cardAID(manager));
                                intent.putExtra("AMOUNT", amount);
                                intent.putExtra("MID", ResponseConstant.DE42);
                                intent.putExtra("TID", ResponseConstant.DE41);
                                intent.putExtra("TXN_ID", "1234567");
                                intent.putExtra("INVOICE", "1111111");
                                intent.putExtra("CARD_TYPE", convertHexToStringValue(DataSetting.applName(manager)));
                                intent.putExtra("APPR_CODE", "123456");
                                intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);
                                startActivity(intent);
                                finish();
                               shutdownAndAwaitTermination(backgroundExecutor);
                            }






                               // ReversalTestApi(DataSetting.getAuthorizedAmount(manager),DataSetting.cardSequenceNumber(manager),tempTrack2Data,"6B409832",PINBLOCK,DataSetting.getAllDataReversal(manager),PANSN,"22");

                           /* Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                            intent.putExtra("flag","success");
                            intent.putExtra("RRN_NO",ResponseConstant.DE37);
                            intent.putExtra("RESPONSE_CODE",ResponseConstant.DE39);
                            intent.putExtra("APP_NAME", "RuPay Debit");
                            intent.putExtra("AID", DataSetting.cardAID(manager));
                            intent.putExtra("AMOUNT", amount);
                            intent.putExtra("MID", ResponseConstant.DE42);
                            intent.putExtra("TID", ResponseConstant.DE41);
                            intent.putExtra("TXN_ID", "1234567");
                            intent.putExtra("INVOICE", "1111111");
                            intent.putExtra("CARD_TYPE", "Rupay");
                            intent.putExtra("APPR_CODE", "123456");
                            intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);
                            startActivity(intent);*/
                        }
                        else {
                           // backgroundExecutor.shutdownNow();
                           // backgroundExecutor.shutdown();

                            Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                            intent.putExtra("flag","failure");
                            intent.putExtra("RRN_NO",ResponseConstant.DE37);
                            intent.putExtra("RESPONSE_CODE",ResponseConstant.DE39);
                            intent.putExtra("APP_NAME", "RuPay Debit");
                            intent.putExtra("AID", DataSetting.cardAID(manager));
                            intent.putExtra("AMOUNT", amount);
                            intent.putExtra("MID", ResponseConstant.DE42);
                            intent.putExtra("TID", ResponseConstant.DE41);
                            intent.putExtra("TXN_ID", "1234567");
                            intent.putExtra("INVOICE", "1111111");
                            intent.putExtra("CARD_TYPE", convertHexToStringValue(DataSetting.applName(manager)));
                            intent.putExtra("APPR_CODE", "123456");
                            intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);
                            startActivity(intent);
                            finish();
                            shutdownAndAwaitTermination(backgroundExecutor);
                        }
                    }
                }
                @Override
                public void onFailure(Call<PosTransResponse> call, Throwable t) {
                    call.cancel();
                    Log.d("Failure", String.valueOf(t));
                }
            });
        }


        /*call.enqueue(new Callback<PosTransResponse>() {
            @Override
            public void onResponse(Call<PosTransResponse> call, Response<PosTransResponse> response) {
                Log.d("SuccessAPI", String.valueOf(response.body()));
                if(response.isSuccessful()){
                    PosTransResponse posTransResponse = response.body();
                    String details = posTransResponse.getStatusDes().toString();
                    TransactionResponseHandler.responseHandler(details);
                        String AuthManager = "";
                        Log.d("MPOS","RESPONSE CODE : " + ResponseConstant.DE39);
                        if((ResponseConstant.DE39).equalsIgnoreCase("0000")){
                            AuthManager = "00";
                        }else{
                            AuthManager ="01";
                        }
                        System.out.println(AuthManager);

                        SetResponseCodeRes = setResponseData(manager,ResponseConstant.DE39.substring(2),AuthManager,ResponseConstant.DE38,ResponseConstant.DE55);
                        Log.d("MPOS","DE 55 DATA:"+ResponseConstant.DE55);

                        int result = completeTransAndGetData(manager);

                        if(ResponseConstant.DE39.substring(2).equalsIgnoreCase(String.valueOf(BankResponse.Approved))){
                            Log.d("MPOS",String.valueOf(result));
                            Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                            intent.putExtra("flag","success");
                            intent.putExtra("RRN_NO",ResponseConstant.DE37);
                            intent.putExtra("RESPONSE_CODE",ResponseConstant.DE39);
                            intent.putExtra("APP_NAME", "RuPay Debit");
                            intent.putExtra("AID", DataSetting.cardAID(manager));
                            intent.putExtra("AMOUNT", amount);
                            intent.putExtra("MID", ResponseConstant.DE42);
                            intent.putExtra("TID", ResponseConstant.DE41);
                            intent.putExtra("TXN_ID", "1234567");
                            intent.putExtra("INVOICE", "1111111");
                            intent.putExtra("CARD_TYPE", "Rupay");
                            intent.putExtra("APPR_CODE", "123456");
                            intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);
                            startActivity(intent);
                        }
                        else {
                            Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                            intent.putExtra("flag","failure");
                            intent.putExtra("RRN_NO",ResponseConstant.DE37);
                            intent.putExtra("RESPONSE_CODE",ResponseConstant.DE39);
                            intent.putExtra("APP_NAME", "RuPay Debit");
                            intent.putExtra("AID", DataSetting.cardAID(manager));
                            intent.putExtra("AMOUNT", amount);
                            intent.putExtra("MID", ResponseConstant.DE42);
                            intent.putExtra("TID", ResponseConstant.DE41);
                            intent.putExtra("TXN_ID", "1234567");
                            intent.putExtra("INVOICE", "1111111");
                            intent.putExtra("CARD_TYPE", "Rupay");
                            intent.putExtra("APPR_CODE", "123456");
                            intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);

                            startActivity(intent);
                        }
                }

            }

            @Override
            public void onFailure(Call<PosTransResponse> call, Throwable t) {
                call.cancel();
                Log.d("Failure", String.valueOf(t));
            }
        });*/


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private JsonObject ApiJsonMap(String authorizedAmount, String cardSequenceNumber, String track2Data, String s1, String pinBlock, String allData, Integer panSlNo) {
        JsonObject jsonObject = new JsonObject();
        try{
            JSONObject obj = new JSONObject();
            obj.put("pansn",panSlNo);
            obj.put("amount",authorizedAmount);
            obj.put("cardSequenceNumber",cardSequenceNumber);
            obj.put("trackData",track2Data);
            obj.put("deviceSerial",s1);
            obj.put("pinblock",pinBlock);
            obj.put("deviceData",allData);
            obj.put("transactionType",transaction_type);
            obj.put("isFallback","no");
            obj.put("txn_card_type",convertHexToStringValue(DataSetting.applName(manager)));
            obj.put("txnLatitude",lactStr);
            obj.put("txnLongitude",LngStr);
            obj.put("paramA",MATMSDKConstant.paramA);
            obj.put("paramB",MATMSDKConstant.paramB);
            obj.put("paramC",MATMSDKConstant.paramC);

            sendDeviceInfo(s1,track2Data);
            //

            /*
            {

"amount":"000000310000",
"cardSequenceNumber":"161844",
"trackData":"167242DF6E58AF7E72A979E1B3967F302CE1B06E49A53A29",
"deviceSerial":"6B409832",
"pinblock":"E57B721271DC90FB",
"deviceData":"9F0206000000310000950580800480009B0268008407A00000052410109F26086963A5CC982E75D7820239009A031912199C01015F2A0203569F33036040009F1A0203569F1E0836423430393833329F2701809F3602002C9F370443C03B419F03060000000000005F3401019F34034203009F10080105A00000000000",
"pansn":"1",
"isFallback":"no"

}
             */


            Log.d("MPOS","Request Data : " + obj.toString());
            JsonParser jsonParser = new JsonParser();
            jsonObject = (JsonObject) jsonParser.parse(obj.toString());
        }catch (JSONException e){
            e.printStackTrace();
        }
        return jsonObject;
    }


    //For fallback
    private JsonObject ApiJsonMapFallback(String s1, String authorizedAmount, String track2Data, String pinBlock, Integer panSn) {
        JsonObject jsonObject = new JsonObject();
        try{
            JSONObject obj = new JSONObject();
            obj.put("amount",authorizedAmount);  //000000610000
            obj.put("trackData",track2Data);
            obj.put("pinblock",pinBlock);
            obj.put("deviceSerial",s1);
            obj.put("transactionType",transaction_type);
            obj.put("pansn", String.valueOf(panSn));
            obj.put ("isFallback","yes");
            obj.put("txn_card_type",convertHexToStringValue(DataSetting.applName(manager)));
            obj.put("txnLatitude",lactStr);
            obj.put("txnLongitude",LngStr);
            obj.put("paramA",MATMSDKConstant.paramA);
            obj.put("paramB",MATMSDKConstant.paramB);
            obj.put("paramC",MATMSDKConstant.paramC);


            Log.d("MPOS","Request Data : " + obj.toString());
            sendDeviceInfo(s1,track2Data);
            JsonParser jsonParser = new JsonParser();
            jsonObject = (JsonObject) jsonParser.parse(obj.toString());
        }catch (JSONException e){
            e.printStackTrace();
        }
        return jsonObject;
    }


    private void ReversalTestApi(String stan,String authorizedAmount, String cardSequenceNumber, String track2Data, String s1, String pinBlock, String allData, Integer panSlNo, String respCode) {


        JSONObject obj = new JSONObject();
        try {
            obj.put("amount", authorizedAmount);
            obj.put("cardSequenceNumber",cardSequenceNumber);
            obj.put("trackData",track2Data);
            obj.put("deviceSerial",s1);
            obj.put("pinblock",pinBlock);
            obj.put("deviceData",allData);
            obj.put("pansn",panSlNo);
            obj.put("respCode",respCode);//respCode
            obj.put("stan",stan);


            Log.d("MPOS :","Requested Data: "+obj.toString());

            AndroidNetworking.post("http://35.200.141.19:8080/IttyMApp/doCashWithdralReversal")
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //api_progress.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                Log.d("MPOS :",obj.toString());
                                String statusDesc = obj.getString("statusDes");

                                TransactionResponseHandler.responseHandler(statusDesc);
                                String AuthManager = "";
                                Log.d("MPOS","RESPONSE CODE : " + ResponseConstant.DE39);
                                if((ResponseConstant.DE39).equalsIgnoreCase("0000")){
                                    AuthManager = "00";
                                }else{
                                    AuthManager ="01";
                                }
                               // System.out.println(AuthManager);
                                SetResponseCodeRes = setResponseData(manager,ResponseConstant.DE39.substring(2),AuthManager,ResponseConstant.DE38,ResponseConstant.DE55);

                                int result = completeTransAndGetData(manager);

                                if(ResponseConstant.DE39.substring(2).equalsIgnoreCase(String.valueOf(BankResponse.Approved))){
                                    Log.d("MPOS", String.valueOf(result));
                                   // backgroundExecutor.shutdownNow();
                                    //backgroundExecutor.shutdown();

                            Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                            intent.putExtra("flag","success");
                            intent.putExtra("RRN_NO",ResponseConstant.DE37);
                            intent.putExtra("RESPONSE_CODE",ResponseConstant.DE39);
                            intent.putExtra("APP_NAME", "RuPay Debit");
                            intent.putExtra("AID", DataSetting.cardAID(manager));
                            intent.putExtra("AMOUNT", amount);
                            intent.putExtra("MID", ResponseConstant.DE42);
                            intent.putExtra("TID", ResponseConstant.DE41);
                            intent.putExtra("TXN_ID", "1234567");
                            intent.putExtra("INVOICE", "1111111");
                            intent.putExtra("CARD_TYPE", "Rupay");
                            intent.putExtra("APPR_CODE", "123456");
                            intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);
                            startActivity(intent);
                            shutdownAndAwaitTermination(backgroundExecutor);
                            finish();
                                   // backgroundExecutor.shutdown();
                                }
                                else{
                                   // backgroundExecutor.shutdownNow();
                                    //backgroundExecutor.shutdown();

                                    Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                                    intent.putExtra("flag","failure");
                                    intent.putExtra("RRN_NO",ResponseConstant.DE37);
                                    intent.putExtra("RESPONSE_CODE",ResponseConstant.DE39);
                                    intent.putExtra("APP_NAME", "RuPay Debit");
                                    intent.putExtra("AID", DataSetting.cardAID(manager));
                                    intent.putExtra("AMOUNT", amount);
                                    intent.putExtra("MID", ResponseConstant.DE42);
                                    intent.putExtra("TID", ResponseConstant.DE41);
                                    intent.putExtra("TXN_ID", "1234567");
                                    intent.putExtra("INVOICE", "1111111");
                                    intent.putExtra("CARD_TYPE", "Rupay");
                                    intent.putExtra("APPR_CODE", "123456");
                                    intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);
                                    startActivity(intent);
                                    finish();
                                    shutdownAndAwaitTermination(backgroundExecutor);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("MPOS :",anError.getErrorBody());

                        }
                    });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //--------------
    public static String convertHexToStringValue(String hex) {
        StringBuilder stringbuilder = new StringBuilder();
        char[] hexData = hex.toCharArray();
        for (int count = 0; count < hexData.length - 1; count += 2) {
            int firstDigit = Character.digit(hexData[count], 16);
            int lastDigit = Character.digit(hexData[count + 1], 16);
            int decimal = firstDigit * 16 + lastDigit;
            stringbuilder.append((char)decimal);
        }
        return stringbuilder.toString();
    }

    public void sendDeviceInfo(final String devicename, final String card_no){
        try {
            JSONObject obj = new JSONObject();
            obj.put("device_sn", devicename.replace("D180-","").trim());
            obj.put("retailer_id", Integer.valueOf(EnvData.user_id));
            obj.put("card_no", card_no);

            AndroidNetworking.post("https://app3.iserveu.tech/validate_bulk_matm")
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");

                                if(status.equalsIgnoreCase("1")){


                                }else{
                                  }
                           /* resetPairedDeviceUI();
                            updateBluetoothList();
                            onBackPressed();*/


                            } catch (JSONException e) {
                                e.printStackTrace();
                               }

                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("Error  "+anError.getErrorDetail());
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    private void checkUserDetails(){

        loadingView.show();
        String url = "https://apps.iserveu.online/get/result/"+ MATMSDKConstant.paramA+"/"+MATMSDKConstant.paramB;
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //pd.dismiss();
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            String msg = obj.getString("statusDesc");

                            if(status.equalsIgnoreCase("0")){
                                getUserAuthToken();
                            }else{
                                if(loadingView!=null){
                                    loadingView.dismiss();
                                }
                                showAlert(msg);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        loadingView.dismiss();

                        showAlert("Invalid User");

                    }

                });

    }
    private void getUserAuthToken(){
        loadingView.show();
        //String url = "http://34.93.9.53:8080/matm/api/getAuthenticateData";
        String url = MATMSDKConstant.BASE_URL+"/api/getAuthenticateData" ;
        JSONObject obj = new JSONObject();
        try {
            obj.put("encryptedData",MATMSDKConstant.encryptedData);
            obj.put("retailerUserName",MATMSDKConstant.loginID);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");

                                if(status.equalsIgnoreCase("success")) {
                                    String userName = obj.getString("username");
                                    String userToken = obj.getString("usertoken");
                                    session.setUsername(userName);
                                    session.setUserToken(userToken);
                                   // loadingView.dismiss();
                                    EnvData.token = userToken;

                                    getUserId(session.getUserToken(),"https://mobile.9fin.co.in/user/user_details");
                                   // getUserId(session.getUserToken(),"http://34.93.9.53:8080/matm/user/user_details");



                                }else {
                                    showAlert(status);
                                    loadingView.dismiss();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                loadingView.dismiss();
                                showAlert("Invalid Encrypted Data");
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            loadingView.dismiss();
                        }

                    });
        }catch ( Exception e){
            e.printStackTrace();
        }
    }
    public void getUserId(String token, String urlString){
        AndroidNetworking.get(urlString)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization",token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loadingView.dismiss();
                            JSONObject obj = new JSONObject(response.toString());
                            String id = obj.getString("id");
                            EnvData.user_id = id;
                            System.out.println(obj);
                            //=================
                            initView();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        loadingView.dismiss();
                        System.out.println("Error  "+anError.getErrorDetail());
                    }
                });
    }
    public void showAlert(String msg){

        AlertDialog.Builder builder = new AlertDialog.Builder(PosActivity.this);
        builder.setTitle("Alert!!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.show();
    }

    void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                pool.shutdownNow();
                 if (!pool.awaitTermination(60, TimeUnit.SECONDS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            pool.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    private void retriveUserList() {
        final PackageManager packageManager = this.getPackageManager();
        Intent intent = getIntent();
        List<ResolveInfo> packages = packageManager.queryIntentActivities(intent,0);

        String pkgName = "";
        for(ResolveInfo res : packages){
            pkgName = res.activityInfo.packageName;
            Log.w("Package Name: ",pkgName);
        }
        if(pkgName.equalsIgnoreCase("com.example.annapurna_aeps")|| pkgName.equalsIgnoreCase("com.example.midland_microfin")|| pkgName.equalsIgnoreCase("com.pax.pax_sdk_app")||pkgName.equalsIgnoreCase("com.jayam.impactapp")||pkgName.equalsIgnoreCase("com.isu.coreapp")){
            System.out.println("Allow"); //com.pax.pax_sdk_app
            getUserAuthToken();

        }else{
            showAlert("Package name does not resister.");
        }
        //-------------------------------------------------------------

     /*   FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("user");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap hashUser = (HashMap) dataSnapshot.getValue();
                if (hashUser != null) {
                    String Drinks = hashUser.get("package1").toString();
                    Toast.makeText(UPIHomeActivity.this, Drinks , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(UPIHomeActivity.this, "Database error." , Toast.LENGTH_SHORT).show();

            }
        });*/






        /*FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("user")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("Rajesh", document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.d("Rajesh", "Error getting documents: ", task.getException());
                        }
                    }
                });*/
    }

}
